﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Pencil
    {
        private string dictionaryString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private Random random = new Random();

        public char GetRandomChar()
        {
            
            return dictionaryString[random.Next(0, dictionaryString.Length)];
        }

        public char Delete()
        {
            return ' ';
        }

    }
}
