﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class PencilManager
    {
        private Pencil pencil;
        private Board board;
        private Random rand = new Random();

        public PencilManager(Pencil pen, Board brd)
        {
            pencil = pen;
            board = brd;
        }

        public int DrawNextChar()
        {
            int y;
            char c = ' ';
            int x = rand.Next(0, board.Current.Length);

            y = board.Current[x];

            if (!(y == board.Y - 1))
            {
                c = pencil.GetRandomChar();
                board.Fields[x, y + 1] = c;
                board.Current[x] = y + 1;
                Draw(x, (y + 1), c);
                return -1;
            }
            else
                return x;

        }

        public void DrawStart()
        {
            int y;
            char c;

            for (int x = 0; x < board.Current.Length; x++)
            {
                y = board.Start[x];
                c = pencil.GetRandomChar();
                board.Fields[x, y] = c;
                Draw(x, y, c);
            }
        }

        public int Rubberise(int x = -1)
        {
            bool wait = true;

            if (x == -1)
                x = rand.Next(0, board.Current.Length);
            else
                wait = false;

            for (int y = board.Y - 1; y >= 0; y--)
            {
                Draw(x, y, ' ');
                int rnd = rand.Next(0, board.Y);
                board.Current[x] = rnd;
                board.Start[x] = rnd;

                if (wait)
                    Thread.Sleep(30);
            }

            return x;
        }

        private void Draw(int x, int y, char c)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(c);
        }

        public void ReDraw()
        {
            int randNum = rand.Next(0, board.Current.Length);
            int current = 0;
            int start = 0;
            int chosenPos = 0;
            char c = ' ';

            randNum = rand.Next(0, board.Current.Length);
            current = board.Current[randNum];
            start = board.Start[randNum];
            chosenPos = rand.Next(start, current);
            c = pencil.GetRandomChar();
            board.Fields[randNum, chosenPos] = c;
            Draw(randNum, chosenPos, c);
        }
    }
}
