﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class ThreadRunner
    {
        private PencilManager manager;
        private Board board;

        public ThreadRunner(PencilManager manager, Board brd)
        {
            this.manager = manager;
            board = brd;
        }

        public void ReDraw()
        {
            while (true)
            {
                manager.ReDraw();
                Thread.Sleep(15);
            }
        }

        public void Rubberise()
        {
            int x = 0;
            while (true)
            {
                x = manager.Rubberise();
                board.SetNewStart(x);
                Thread.Sleep(80);
            }
        }

    }
}

