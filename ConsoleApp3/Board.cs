﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Board
    {
        private int[] start;
        private int[] current;
        private int[,] fields;
        public int X { get; }
        public int Y { get; }

        public int[,] Fields { get => fields; set => fields = value; }
        public int[] Start { get => start; set => start = value; }
        public int[] Current { get => current; set => current = value; }

        private Random random = new Random();

        public Board(int x, int y)
        {
            this.X = x;
            this.Y = y;
            fields = new int[x, y];
            Start = new int[x];
            Current = new int[x];
        }

        public void SetNewStart(int x)
        {
            int b = 0;

            b = random.Next(0, Y);

            start[x] = b;
            current[x] = b;
        }

        public void GenerateStartPos()
        {
            for (int a = 0; a < X; a++)
            {
                SetNewStart(a);
            }
        }
    }
}
