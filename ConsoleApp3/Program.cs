﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace ConsoleApp3
{
    class Program
    {
        static int sizeY = 30;
        static int sizeX = 100;

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.CursorVisible = false;
            Console.SetWindowSize(sizeX,sizeY);
            Board brd = new Board(sizeX, sizeY);
            brd.GenerateStartPos();
            Pencil pncl = new Pencil();
            PencilManager manager = new PencilManager(pncl, brd);
            manager.DrawStart();

            ThreadRunner runner = new ThreadRunner(manager, brd);

            Thread reDrawThread = new Thread(new ThreadStart(runner.ReDraw));
            reDrawThread.Start();
            Thread rubberiseThread = new Thread(new ThreadStart(runner.Rubberise));
            rubberiseThread.Start();

            int x = -1;

            while (true)
            {

                x = manager.DrawNextChar();
                if (x!=-1)
                {
                    manager.Rubberise(x);
                    brd.SetNewStart(x);
                    x = -1;
                }
                Thread.Sleep(45);
            }
        }

    }
}
